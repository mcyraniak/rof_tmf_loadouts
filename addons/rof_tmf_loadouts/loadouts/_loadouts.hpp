#include "_macros.hpp"

class CfgLoadouts
{
    class USMC_WD {
        displayName = "USMC (Woodland)";
        category = "Blufor (Modern)";
        #include "usmc_wd.hpp"
    };
    class USMC_D {
        displayName = "USMC (Desert)";
        category = "Blufor (Modern)";
        #include "usmc_d.hpp"
    };
    class US_WD {
        displayName = "US Army (Woodland)";
        category = "Blufor (Old School)";
        #include "us_wd.hpp"
    };
    class FIA_TS {
        displayName = "FIA (Tiger Stripe)";
        category = "3IFB";
        #include "fia_ts.hpp"
    };
    class UN_MO {
        displayName = "U.N. Peacekeepers (Mountain)";
        category = "Independent";
        #include "un_mo.hpp"
    };
    class VDV_EMR {
        displayName = "VDV (EMR)";
        category = "Russians";
        #include "vdv_emr.hpp"
    };
    class VDV_F {
        displayName = "VDV (Flora)";
        category = "Russians";
        #include "vdv_f.hpp"
    };
    class VDV_MF {
        displayName = "VDV (Mountain Flora)";
        category = "Russians";
        #include "vdv_mf.hpp"
    };
};